//
//  TestingData.swift
//  AC Hunting Companion
//
//  Created by Tiago Pereira on 02/06/2020.
//  Copyright © 2020 Tiago Pereira. All rights reserved.
//

import Foundation

class TestingDataManager {
    
    static let testingInsect: Animal = Animal(internalID: 583, critterId: 3, name: "Tiger butterfly", image: "https://acnhcdn.com/latest/BookInsectIcon/InsectAgehachoCropped.png", sell: 240, weather: "Any except rain", category: "Insects", iconImage: "https://acnhcdn.com/latest/MenuIcon/Ins2.png", activeMonths: nil, iconFilename: "Ins2", obtainedFrom: "Flying", shadow: nil)
    static let testingFish: Animal = Animal(internalID: 2251, critterId: 42, name: "Dorado", image: "https://acnhcdn.com/latest/BookFishIcon/FishDoladoCropped.png", sell: 15000, weather: nil, category: "Fish", iconImage: "https://acnhcdn.com/latest/MenuIcon/Fish34.png", activeMonths: nil, iconFilename: "Fish34", obtainedFrom: "River", shadow: "X-Large")
    
    static let testingInsectExtraInfo: AnimalInfo = AnimalInfo(name: "Tiger butterfly", captureQuote: "I caught a tiger butterfly! I've earned my stripes!", blathersDescription: "Tiger butterflies are known for their majestic wings, which many consider quite beautiful. Truth be told, I find them monstrous! Those strange striped patterns... They give this owl the goose bumps! And while you may imagine young tiger butterfly larvae to look like lovely green caterpillars... it's not so! Why, when tiger butterflies are but babes, they're covered in unsightly white, brown, and black spots. In this way, they camouflage themselves as...as...bird droppings! Putrid pests, indeed!")
    static let testingFishExtraInfo:AnimalInfo = AnimalInfo(name: "Dorado", captureQuote: nil, blathersDescription: nil)
    
    private var animals: [Animal] = []
    
    private var animalExtraInfo: [AnimalInfo] = []
    
    init() {
        _ = self.fetchAnimalList()
    }
}

extension TestingDataManager: DataManagerProtocol {
    func fetchExtraInfo(for animal: Animal) -> AnimalInfo? {
        if let infoIndex = animalExtraInfo.firstIndex(where: { $0.name == animal.name }) {
            return animalExtraInfo[infoIndex]
        }
        
        return nil
    }
    
    func fetchAnimalList(ofCategory category: Category = Category.all) -> [Animal] {
        
        if let jsonData = readJSONFile(forName: "TestingData") {
            self.animals = parseAnimalsData(from: jsonData)
        }
        
        switch category {
            case .all:
                return animals
            case .insect:
                return animals.filter { $0.category! == Category.insect.rawValue }
            case .fish:
                return animals.filter { $0.category! == Category.fish.rawValue }
            case .sea:
                return animals.filter { $0.category! == Category.sea.rawValue }
        }
    }
    
    private func parseAnimalsData(from jsonData: Data) -> [Animal] {
        do {
            let results = try JSONDecoder().decode(AnimalResults.self, from: jsonData)
            
            var parsedAnimals = [Animal]()
            
            for animalWrapper in results.results {
                let animal = animalWrapper.content
                print("-- \(animal.name) parsed successfully.")
                parsedAnimals.append(animal)
            }
            
            return parsedAnimals
        } catch {
            print("Decoding error: \(error.localizedDescription)")
        }
        
        return []
    }
}
