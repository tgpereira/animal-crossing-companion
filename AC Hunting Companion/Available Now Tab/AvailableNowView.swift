//
//  AvailableNowView.swift
//  AC Hunting Companion
//
//  Created by Tiago Pereira on 02/06/2020.
//  Copyright © 2020 Tiago Pereira. All rights reserved.
//

import SwiftUI

// TODO: Update every hour

struct AvailableNowView: View {
    
    @EnvironmentObject var viewModel: AnimalListViewModel
    
    var fishesByLocation: [[Animal]] {
        viewModel.fishesByLocationAvailableNow()
    }
    
    var insectsByLocation: [[Animal]] {
        viewModel.insectsByLocationAvailableNow()
    }
    
    var nextHourAnimals: [[Animal]] {
        return viewModel.availableAnimalsComingNextHour()
    }
    
    @State private var selectedAnimalType = 0
    
    let animalTypeLabels = ["Fishes", "Insects", "Coming Next"]
    
    var body: some View {
        NavigationView {
            List {
                // Current Date and List Filter Views
                Section {
                    VStack(alignment: .center) {
                        TodayView()
                    }.padding()
                    
                    Picker(selection: $selectedAnimalType, label: Text("Animal Type")) {
                        ForEach(0 ..< animalTypeLabels.count) { index in
                            Text("\(self.animalTypeLabels[index])")
                        }
                    }.pickerStyle(SegmentedPickerStyle())
                }
                
                // Fishes
                if selectedAnimalType == 0 {
                    ForEach(0 ..< fishesByLocation.count) {
                        AnimalGroupedSectionView(animals: self.fishesByLocation[$0])
                    }
                }
                
                // Insects
                if selectedAnimalType == 1 {
                    ForEach(0 ..< insectsByLocation.count) {
                        AnimalGroupedSectionView(animals: self.insectsByLocation[$0])
                    }
                }
                
                if selectedAnimalType == 2 {
                    if nextHourAnimals.count == 0 {
                        Text("Nothing new coming next...")
                    } else {
                        ForEach(0 ..< nextHourAnimals.count) {
                            AnimalGroupedSectionView(animals: self.nextHourAnimals[$0])
                        }
                    }
                }
                
            }
            .listStyle(GroupedListStyle())
            .environment(\.horizontalSizeClass, .regular)
                
            .navigationBarTitle(Text("Available Now"))
            .navigationBarItems(trailing: HStack {
                Button(action: {
                    self.reloadData()
                    // TODO: a little notification badge appear from the bottom up saying "Data reloaded" or somehting like this
                }) {
                    Image(systemName: "arrow.2.circlepath.circle.fill")
                        .imageScale(.large)
                }
            })
        }
        .onAppear() {
            self.viewModel.fetchAnimals()
        }
    }
}

extension AvailableNowView {
    private func reloadData() {
        viewModel.fetchAnimals()
    }
}

struct AnimalGroupedSectionView: View {
    
    var animals: [Animal] = []
    
    var sectionTitle: String {
        if let firstAnimal = self.animals.first {
            return firstAnimal.obtainedFrom ?? "-"
        }
        
        return "-"
    }
    
    var body: some View {
        Section(header: Text(sectionTitle)) {
            ForEach(animals) { animal in
                AnimalAvailabilityRowView(animal: animal)
            }
        }
    }
}

struct AvailableNowView_Previews: PreviewProvider {
    static var previews: some View {
        let viewModel = AnimalListViewModel(dataManager: TestingDataManager())
        
        return Group {
            AvailableNowView().environmentObject(viewModel)
                .environment(\.colorScheme, .light)
            
            AvailableNowView().environmentObject(viewModel)
                .environment(\.colorScheme, .dark)
        }
        
    }
}
