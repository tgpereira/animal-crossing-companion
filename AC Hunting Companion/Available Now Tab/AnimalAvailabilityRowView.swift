//
//  InsectRowView.swift
//  AC Hunting Companion
//
//  Created by Tiago Pereira on 02/06/2020.
//  Copyright © 2020 Tiago Pereira. All rights reserved.
//

import SwiftUI

struct AnimalAvailabilityRowView: View {
    
    var animal: Animal
    
    @Environment(\.imageCache) var cache: ImageCache
    
    var body: some View {
        HStack(alignment: .center) {
            AsyncImage(url: animal.iconURL!,
                       placeholder: RoundedRectangle(cornerRadius: 12),
                       cache: self.cache
            ).frame(width: 50, height: 50)
            
            VStack(alignment: .leading) {
                Text(animal.name)
                    .font(.headline)
                
                if animal.category == Category.fish.rawValue {
                    Text("\(animal.shadow ?? "-") shadow").font(.caption) +
                    Text(" | \(self.formatAvailableTime(for: animal))").font(.caption)
                    
                } else {
                    Text("\(self.formatAvailableTime(for: animal))")
                        .font(.caption)
                }
            }
            
            Spacer()
            
            HStack {
                if animal.isLastAvailableHour {
                    Image(systemName: "clock.fill")
                        .foregroundColor(.yellow)
                }
                Image(systemName: "dollarsign.circle")
                Text("\(animal.sell ?? 0)")
            }
            
        }
        
    }
}

// MARK: - Helper Functions

extension AnimalAvailabilityRowView {
    
    private func getCurrentMonth() -> String {
        let now = Date()
        let formatter = DateFormatter()
        formatter.setLocalizedDateFormatFromTemplate("M")
        let currentMonth = formatter.string(from: now)
        
        return String(Int(currentMonth)! - 1)
    }
    
    private func formatAvailableTime(for animal: Animal) -> String {
        func format(activeMonth: ActiveMonth) -> String {
            if activeMonth.activeTimes[0] == "0" {
                return "All day"
            } else {
                return "\(activeMonth.activeTimes[0]) - \(activeMonth.activeTimes[1])"
            }
        }
        
        if let activeMonths = animal.activeMonths {
            let currentMonth = getCurrentMonth()
            
            if let animalActiveMonth = activeMonths[currentMonth] {
                return format(activeMonth: animalActiveMonth)
            }
        }
        
        return "-"
    }
}

struct InsectRowView_Previews: PreviewProvider {
    static var previews: some View {
        let testingInsect = TestingDataManager.testingInsect
        let testingFish = TestingDataManager.testingFish
        
        return Group {
            AnimalAvailabilityRowView(animal: testingInsect)
                .previewLayout(.fixed(width: 375, height: 70))
                .environment(\.colorScheme, .light)
            
            AnimalAvailabilityRowView(animal: testingFish)
                .previewLayout(.fixed(width: 375, height: 70))
                .environment(\.colorScheme, .light)
            
            AnimalAvailabilityRowView(animal: testingInsect)
                .previewLayout(.fixed(width: 375, height: 70))
                .environment(\.colorScheme, .dark)
        }
        
    }
}
