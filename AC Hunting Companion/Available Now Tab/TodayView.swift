//
//  TodayView.swift
//  AC Hunting Companion
//
//  Created by Tiago Pereira on 02/06/2020.
//  Copyright © 2020 Tiago Pereira. All rights reserved.
//

import SwiftUI

// TODO: Update the time every hour

struct TodayView: View {
    
    var today: Date {
        return Date()
    }
    
    var body: some View {
        Group {
            Text("\(format(date: today))")
                .font(.headline)
        }
    }
    
    func format(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.setLocalizedDateFormatFromTemplate("EEEE, dd MMM yyyy, hh")
        let formattedDate = formatter.string(from: date)
        
        return formattedDate
    }
}

struct TodayView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            TodayView()
                .previewLayout(.fixed(width: 375, height: 70))
                .environment(\.colorScheme, .light)
            
            TodayView()
                .previewLayout(.fixed(width: 375, height: 70))
                .environment(\.colorScheme, .dark)
            
        }
    }
}
