//
//  AnimalCrossingCrittersService.swift
//  AC Hunting Companion
//
//  Created by Tiago Pereira on 02/06/2020.
//  Copyright © 2020 Tiago Pereira. All rights reserved.
//

import Foundation

func readJSONFile(forName name: String) -> Data? {
    do {
        if let bundlePath = Bundle.main.path(forResource: name, ofType: "json"),
            let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
            return jsonData
        }
    } catch {
        print("File \(name): \(error.localizedDescription)")
    }
    
    return nil
}
