//
//  DataManager.swift
//  AC Hunting Companion
//
//  Created by Tiago Pereira on 02/06/2020.
//  Copyright © 2020 Tiago Pereira. All rights reserved.
//

import Foundation

protocol DataManagerProtocol {
    func fetchAnimalList(ofCategory category: Category) -> [Animal]
    func fetchExtraInfo(for animal: Animal) -> AnimalInfo?
}

extension DataManagerProtocol {
    func fetchAnimalList(ofCategory category: Category = Category.all) -> [Animal] {
        return fetchAnimalList(ofCategory: category)
    }
}

class DataManager {
    static let shared: DataManagerProtocol = DataManager()
    
    private var animals = [Animal]()
    private var insects = [Animal]()
    private var fishes = [Animal]()
    private var seaCreatures = [Animal]()
    
    private var animalExtraInfo = [AnimalInfo]()
}

extension DataManager: DataManagerProtocol {
    func fetchAnimalList(ofCategory category: Category = Category.all) -> [Animal] {
        
        self.animals = []
        
        switch category {
            case .insect:
                // Can extract a function
                if let jsonData = readJSONFile(forName: Category.fileName(for: .insect)) {
                    self.insects = parseAnimalsData(from: jsonData)
                    
                    if let jsonData = readJSONFile(forName: "bugs-quotes") {
                        self.animalExtraInfo = parseAnimalExtraInfoData(from: jsonData)
                    }
                }
                
                return self.insects
            
            case .fish:
                // Can extract a function
                if let jsonData = readJSONFile(forName: Category.fileName(for: .fish)) {
                    self.fishes = parseAnimalsData(from: jsonData)
                }
                
                return self.fishes
            
            case .sea:
                // Can extract a function
                if let jsonData = readJSONFile(forName: Category.fileName(for: .sea)) {
                    self.seaCreatures = parseAnimalsData(from: jsonData)
                }
                
                return self.seaCreatures
            case .all:
                var parsedInsects = [Animal]()
                var parsedFish = [Animal]()
                var parsedSeaCreatures = [Animal]()
                
                if let jsonData = readJSONFile(forName: Category.fileName(for: .insect)) {
                    parsedInsects = parseAnimalsData(from: jsonData)
                    
                    if let jsonData = readJSONFile(forName: "bugs-quotes") {
                        self.animalExtraInfo = parseAnimalExtraInfoData(from: jsonData)
                    }
                }
                
                if let jsonData = readJSONFile(forName: Category.fileName(for: .fish)) {
                    parsedFish = parseAnimalsData(from: jsonData)
                }
                
                if let jsonData = readJSONFile(forName: Category.fileName(for: .sea)) {
                    parsedSeaCreatures = parseAnimalsData(from: jsonData)
                }
                
                self.animals.append(contentsOf: parsedInsects)
                self.animals.append(contentsOf: parsedFish)
                self.animals.append(contentsOf: parsedSeaCreatures)
                
                return self.animals
        }
    }
    
    private func parseAnimalsData(from jsonData: Data) -> [Animal] {
        do {
            let results = try JSONDecoder().decode(AnimalResults.self, from: jsonData)
            
            var parsedAnimals = [Animal]()
            
            for animalWrapper in results.results {
                let animal = animalWrapper.content
                // print("-- \(animal.name) parsed successfully.")
                parsedAnimals.append(animal)
            }
            
            return parsedAnimals
        } catch {
            print("[parseAnimalsData] Decoding error: \(error.localizedDescription)")
        }
        
        return []
    }
    
    private func parseAnimalExtraInfoData(from jsonData: Data) -> [AnimalInfo] {
        do {
            let results = try JSONDecoder().decode(AnimalInfoResults.self, from: jsonData)
            
            var parsedAnimalsInfo = [AnimalInfo]()
            
            for animalInfo in results.results {
                // print("-- \(animalInfo.name) extra info parsed successfully.")
                parsedAnimalsInfo.append(animalInfo)
            }
            
            return parsedAnimalsInfo
        } catch {
            print("[parseAnimalExtraInfoData] Decoding error: \(error.localizedDescription)")
        }
        
        
        return []
    }
    
    func fetchExtraInfo(for animal: Animal) -> AnimalInfo? {
        if let infoIndex = animalExtraInfo.firstIndex(where: { $0.name == animal.name }) {
            return animalExtraInfo[infoIndex]
        }
        
        return nil
    }
}
