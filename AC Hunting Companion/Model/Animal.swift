//
//  Animal.swift
//  AC Hunting Companion
//
//  Created by Tiago Pereira on 02/06/2020.
//  Copyright © 2020 Tiago Pereira. All rights reserved.
//

import Foundation

enum Category: String, Codable {
    case insect = "Insects"
    case fish = "Fish"
    case sea = "Sea Creatures"
    case all
    
    static func fileName(for category: Category) -> String {
        switch category {
            case .insect:
                return "bugs"
            case .fish:
                return "fish"
            case .sea:
                return "sea"
            default:
                return "bugs"
        }
    }
}

struct ActiveMonth: Codable, Equatable {
    let activeTimes: [String]
}

extension ActiveMonth {
    var beginning: Int {
        if let hour = Date.convertHourToInt(activeTimes[0]) {
            return hour
        }
        
        return -1
    }
    
    var ending: Int {
        if let hour = Date.convertHourToInt(activeTimes[1]) {
            return hour
        }
        
        return -1
    }
}

struct Animal: Codable, Identifiable {
    
    var id: String {
        return self.name
    }
    
    let internalID: Int?
    let critterId: Int?
    
    let name: String
    
    // Photo of the animal
    let image: String?
    
    let sell: Int?
    // Insect Only
    let weather: String?
    let category: String?
    
    // Image that appears in your pocket
    let iconImage: String?
    
    let activeMonths: [String: ActiveMonth]?
    
    let iconFilename:  String?
    let obtainedFrom:  String?
    
    // Fish Only
    let shadow: String?
    
    // TODO: Add it to the lists
    // - At the JSON for fishes, sometimes its a String, other times are Int
    // let spawnRates: String?
}

extension Animal {
    
    var activeMonthsList: [String] {
        if let monthsList = self.activeMonths?.keys.map({ String($0) }) {
            return monthsList
        }
        
        return []
    }
    
    // Months go from 0 to 11
    func availableTimes(for month: String) -> ActiveMonth? {
        if let activeMonths = self.activeMonths {
            return activeMonths[month]
        }
        
        return nil
    }
    
    func beginningAvailableHour(for month: String) -> Int? {
        if let availableTimes = self.availableTimes(for: month) {
            return availableTimes.beginning
        }
        
        return nil
    }
    
    func endingAvailableHour(for month: String) -> Int? {
        if let availableTimes = self.availableTimes(for: month) {
            return availableTimes.ending
        }
        
        return nil
    }
    
    var isAvailableThisMonth: Bool {
        let now = Date()
        let currentMonth = "\((now.currentMonth ?? 0) - 1)"
        
        return self.isAvailable(at: currentMonth)
    }
    
    func isAvailable(at month: String) -> Bool {
        for activeMonth in self.activeMonthsList {
            if month == activeMonth {
                return true
            }
        }
        
        return false
    }
    
    var isAvailableNow: Bool {
        if self.isAvailableThisMonth {
            let now = Date()
            let currentMonth = "\((now.currentMonth ?? 0) - 1)"
            let currentHour = now.currentHour ?? 0
            
            if let availableTimes = self.availableTimes(for: currentMonth) {
                if availableTimes.beginning == 0 {
                    return true
                }
                
                // Current hour is after the 'beginning' time and before the 'ending' time
                if availableTimes.beginning < availableTimes.ending {
                    return (currentHour >= availableTimes.beginning) &&
                        (currentHour < availableTimes.ending)
                } else {
                    // "4pm" to "9am" cases are "16" to "9"
                    return (currentHour >= availableTimes.beginning) ||
                        (currentHour < availableTimes.ending)
                }
            }
        }
        
        return false
    }
    
    var iconURL: URL? {
        if let url = URL(string: self.iconImage ?? "") {
            return url
        }
        
        return nil
    }
    
    var imageURL: URL? {
        if let url = URL(string: self.image ?? "") {
            return url
        }
        
        return nil
    }
    
    var isLastAvailableHour: Bool {
        let now = Date()
        let currentMonth = "\((now.currentMonth ?? 0) - 1)"
        
        if let endingAvailableHour = self.endingAvailableHour(for: currentMonth),
            let currentHour = Date().currentHour {
            return endingAvailableHour != 0 &&
                self.isAvailableNow &&
                endingAvailableHour == (currentHour + 1)
        }
        
        return false
    }
    
    var isComingOnTheNextHour: Bool {
        let now = Date()
        let currentMonth = "\((now.currentMonth ?? 0) - 1)"
        
        if let beginningAvailableHour = self.beginningAvailableHour(for: currentMonth),
            let currentHour = Date().currentHour {
            let nextHour = currentHour + 1
            return (beginningAvailableHour == nextHour)
        }
        
        return false
    }
}

struct AnimalResults: Codable {
    let total: Int
    let results: [AnimalWrapper]
    
    public struct AnimalWrapper: Codable {
        public let id: Int
        public let name: String
        public let category: Category
        public var content: Animal
    }
}
