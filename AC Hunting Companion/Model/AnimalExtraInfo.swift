//
//  AnimalExtraInfo.swift
//  AC Hunting Companion
//
//  Created by Tiago Pereira on 10/06/2020.
//  Copyright © 2020 Tiago Pereira. All rights reserved.
//

import Foundation

struct AnimalInfoResults: Codable {
    let total: Int
    let results: [AnimalInfo]
}


struct AnimalInfo: Codable, Identifiable  {
    
    var id: String {
        return name
    }
    
    var name: String
    
    var captureQuote: String?
    var blathersDescription: String?
}
