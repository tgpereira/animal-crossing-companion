//
//  AnimalListViewModel.swift
//  AC Hunting Companion
//
//  Created by Tiago Pereira on 02/06/2020.
//  Copyright © 2020 Tiago Pereira. All rights reserved.
//

import Foundation
import Combine


protocol AnimalListViewModelProtocol {
    var animals: [Animal] { get }
    func fetchAnimals()
    func fetchInsects()
    func fetchFishes()
    func fetchSeaCreatures()
}

final class AnimalListViewModel: ObservableObject {
    @Published var animals: [Animal] = []
    
    @Published var insects: [Animal] = []
    @Published var fishes: [Animal] = []
    @Published var seaCreatures: [Animal] = []
    
    var dataManager: DataManagerProtocol
    
    init(dataManager: DataManagerProtocol = DataManager.shared) {
        self.dataManager = dataManager
        fetchAnimals()
    }
}

extension AnimalListViewModel: AnimalListViewModelProtocol {
    func fetchAnimals() {
        if animals.isEmpty {
            animals = dataManager.fetchAnimalList(ofCategory: .all)
            
            fetchInsects()
            fetchFishes()
            fetchSeaCreatures()
        }
    }
    
    func fetchFishes() {
        fishes = dataManager.fetchAnimalList(ofCategory: .fish)
    }
    
    func fetchInsects() {
        insects = dataManager.fetchAnimalList(ofCategory: .insect)
    }
    
    func fetchSeaCreatures() {
        seaCreatures = dataManager.fetchAnimalList(ofCategory: .sea)
    }
}

extension AnimalListViewModel {
    
    // Not really being used yet
    enum GroupingCriteria: String {
        case location = "obtainedFrom"
    }
    
    func animals(of category: Category, groupedBy criteria: GroupingCriteria) -> [[Animal]] {
        let animals: [Animal]
        
        switch category {
            case .insect:
                animals = self.insects
            case .fish:
                animals = self.fishes
            default:
                animals = self.animals
        }
        
        var animalsByCriteriaDictionary: [String: [Animal]] = [String: [Animal]]()
        
        for animal in animals {
            let animalLocation = animal.obtainedFrom
            
            if let location = animalLocation {
                if animalsByCriteriaDictionary.keys.contains(location) {
                    animalsByCriteriaDictionary[location]?.append(animal)
                } else {
                    animalsByCriteriaDictionary[location] = []
                    animalsByCriteriaDictionary[location]?.append(animal)
                }
            }
        }
        
        var animalsByCriteria: [[Animal]] = []
        
        for (_, value) in animalsByCriteriaDictionary {
            animalsByCriteria.append(value.sorted(by: { $0.sell! > $1.sell! }))
        }
        
        return animalsByCriteria.sorted { $0[0].obtainedFrom! < $1[0].obtainedFrom! }
    }
    
    func fishesByLocationAvailableNow() -> [[Animal]] {
        
        return self.animals(of: .fish, groupedBy: .location)
            // Filters the arrays based on availability
            .map { $0.filter { $0.isAvailableNow } }
            // Filters empty arrays
            .filter { !$0.isEmpty }
    }
    
    func insectsByLocationAvailableNow() -> [[Animal]] {
        
        return self.animals(of: .insect, groupedBy: .location)
            // Filters the arrays based on availability
            .map { $0.filter { $0.isAvailableNow } }
            // Filters empty arrays
            .filter { !$0.isEmpty }
    }
    
    func availableAnimalsComingNextHour() -> [[Animal]] {
        return self.animals(of: .all, groupedBy: .location)
            // Filters the arrays based on availability
            .map { $0.filter { $0.isAvailableThisMonth && $0.isComingOnTheNextHour } }
            // Filters empty arrays
            .filter { !$0.isEmpty }
    }
    
    func newAnimalsAvailableNextMonth() -> [Animal] {
        if let currentMonth = Date().currentMonth {
            let nextMonth = "\(currentMonth + 1)"
            return self.animals.filter {
                $0.isAvailableThisMonth == false &&
                $0.isAvailable(at: nextMonth)
            }
        }
        
        return []
    }
    
    func getExtraInfo(for animal: Animal) -> AnimalInfo? {
        return dataManager.fetchExtraInfo(for: animal)
    }
    
    var animalsLeavingSoon: [Animal] {
        self.animals.filter { $0.isLastAvailableHour }
    }
    
}
