//
//  TabeBarNavigationView.swift
//  AC Hunting Companion
//
//  Created by Tiago Pereira on 04/06/2020.
//  Copyright © 2020 Tiago Pereira. All rights reserved.
//

import SwiftUI

struct TabBarNavigationView: View {
    
    @EnvironmentObject var viewModel: AnimalListViewModel
    
    var body: some View {
        TabView {
            AvailableNowView()
                .tabItem {
                    Image(systemName: "clock.fill")
                    Text("Available Now")
            }
            
            AnimalCatalogView()
                .tabItem {
                    Image(systemName: "book.fill")
                    Text("Catalog")
            }
            
            NextMonthAnimalsView()
                .tabItem {
                    Image(systemName: "calendar")
                    Text("Next Month")
            }
            
        }
    }
}

struct TabeBarNavigationView_Previews: PreviewProvider {
    static var previews: some View {
        let viewModel = AnimalListViewModel(dataManager: TestingDataManager())
        
        return Group {
            TabBarNavigationView().environmentObject(viewModel)
                .environment(\.colorScheme, .light)
            
            TabBarNavigationView().environmentObject(viewModel)
                .environment(\.colorScheme, .dark)
        }
        
    }
}
