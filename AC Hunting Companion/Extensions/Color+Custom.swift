//
//  Color+Custom.swift
//  AC Hunting Companion
//
//  Created by Tiago Pereira on 05/06/2020.
//  Copyright © 2020 Tiago Pereira. All rights reserved.
//

import SwiftUI

extension Color {
    static let primaryColor = Color("primaryColor")
    static let secondaryColor = Color("secondaryColor")
    
    static let cardShadow = Color("cardShadow")
    
    static let primaryBackground = Color("primaryBackground")
    static let secondaryBackground = Color("secondaryBackground")
    
    static let toggleButtonLabelOn = Color("toggleButtonLabelOn")
    static let toggleButtonLabelOff = Color("toggleButtonLabelOff")
}
