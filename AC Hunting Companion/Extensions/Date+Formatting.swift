//
//  Date+Formatting.swift
//  AC Hunting Companion
//
//  Created by Tiago Pereira on 07/06/2020.
//  Copyright © 2020 Tiago Pereira. All rights reserved.
//

import Foundation

extension Date {
    var currentMonth: Int? {
        let monthFormatter = DateFormatter()
        monthFormatter.setLocalizedDateFormatFromTemplate("M")
        
        if let currentMonth = Int(monthFormatter.string(from: self)) {
            return currentMonth
        }

        return nil
    }
    
    var currentHour: Int? {
        let hourFormatter = DateFormatter()
        hourFormatter.setLocalizedDateFormatFromTemplate("H")
        
        if let currentHour = Int(hourFormatter.string(from: self)) {
            return currentHour
        }
        
        return nil
    }
    
    static func convertHourToInt(_ hour: String) -> Int? {
        var convertedHour: Int?
        
        switch hour {
            case "1am":
                convertedHour = 1
            case "2am":
                convertedHour = 2
            case "3am":
                convertedHour = 3
            case "4am":
                convertedHour = 4
            case "5am":
                convertedHour = 5
            case "6am":
                convertedHour = 6
            case "7am":
                convertedHour = 7
            case "8am":
                convertedHour = 8
            case "9am":
                convertedHour = 9
            case "10am":
                convertedHour = 10
            case "11am":
                convertedHour = 11
            case "1pm":
                convertedHour = 13
            case "2pm":
                convertedHour = 14
            case "3pm":
                convertedHour = 15
            case "4pm":
                convertedHour = 16
            case "5pm":
                convertedHour = 17
            case "6pm":
                convertedHour = 11
            case "7pm":
                convertedHour = 19
            case "8pm":
                convertedHour = 20
            case "9pm":
                convertedHour = 21
            case "10pm":
                convertedHour = 22
            case "11pm":
                convertedHour = 23
            default:
                convertedHour = nil
        }
        
        return convertedHour
    }
}
