//
//  NextMonthAnimalsView.swift
//  AC Hunting Companion
//
//  Created by Tiago Pereira on 07/06/2020.
//  Copyright © 2020 Tiago Pereira. All rights reserved.
//

import SwiftUI

struct NextMonthAnimalsView: View {
    @EnvironmentObject var viewModel: AnimalListViewModel
    
    var animals: [Animal] {
        return viewModel.newAnimalsAvailableNextMonth()
            .sorted { $0.sell! > $1.sell! }
    }
    
    var body: some View {
        NavigationView {
            List {
                ForEach(self.animals) { animal in
                    AnimalAvailabilityRowView(animal: animal)
                }
            }
            .listStyle(GroupedListStyle())
            .environment(\.horizontalSizeClass, .regular)
            .navigationBarTitle(Text("Coming Next Month"))
        }
    }
}

struct NextMonthAnimalsView_Previews: PreviewProvider {
    static var previews: some View {
        let viewModel = AnimalListViewModel(dataManager: TestingDataManager())
        
        return Group {
            NextMonthAnimalsView()
                .environmentObject(viewModel)
                .environment(\.colorScheme, .light)
            
            NextMonthAnimalsView()
                .environmentObject(viewModel)
                .environment(\.colorScheme, .dark)
        }
        
    }
}
