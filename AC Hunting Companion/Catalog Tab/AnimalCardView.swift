//
//  AnimalCardView.swift
//  AC Hunting Companion
//
//  Created by Tiago Pereira on 05/06/2020.
//  Copyright © 2020 Tiago Pereira. All rights reserved.
//

import SwiftUI

struct AnimalInfoRow: View {
    
    var animal: Animal
    
    var body: some View {
        HStack {
            AsyncImage(url: animal.iconURL!, placeholder: RoundedRectangle(cornerRadius: 12))
                .frame(width: 50, height: 50)
            
            Text("\(animal.name)")
            
            Spacer()
        }
    }
}

struct AnimalCardView_Previews: PreviewProvider {
    static var previews: some View {
        let testingInsect = Animal(internalID: 0, name: "Violin beetle", image: nil, sell: nil, weather: nil, category: nil, iconImage: nil, activeMonths: nil, iconFilename: nil, obtainedFrom: nil, shadow: nil)
        return AnimalInfoRow(animal: testingInsect)
    }
}
