//
//  AnimalCardView.swift
//  AC Hunting Companion
//
//  Created by Tiago Pereira on 05/06/2020.
//  Copyright © 2020 Tiago Pereira. All rights reserved.
//

import SwiftUI

struct AnimalInfoRowView: View {
    @Environment(\.imageCache) var cache: ImageCache
    
    var animal: Animal
    
    var body: some View {
        HStack {
            AsyncImage(url: animal.iconURL!,
                       placeholder: RoundedRectangle(cornerRadius: 12),
                       cache: self.cache
            ).frame(width: 50, height: 50)
            
            VStack(alignment: .leading) {
                Text("\(animal.name)").font(.headline)
                Text("\(animal.obtainedFrom ??  "-")").font(.subheadline)
            }
            
            Spacer()
            
//            HStack {
//                Button(action: {
//                    print("Touched \(self.animal.name)!")
//                }, label: {
//                    Image(systemName: "circle")
//                        .imageScale(.large)
//                })
//            }
        }
    }
}

struct AnimalInfoRowView_Previews: PreviewProvider {
    static var previews: some View {
        let testingInsect = TestingDataManager.testingFish
        
        return Group {
            AnimalInfoRowView(animal: testingInsect)
                .previewLayout(.fixed(width: 375, height: 70))
                .environment(\.colorScheme, .light)
            
            AnimalInfoRowView(animal: testingInsect)
                .previewLayout(.fixed(width: 375, height: 70))
                .environment(\.colorScheme, .dark)
        }
        
    }
}
