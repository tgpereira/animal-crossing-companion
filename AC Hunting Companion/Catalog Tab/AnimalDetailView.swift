//
//  AnimalDetailView.swift
//  AC Hunting Companion
//
//  Created by Tiago Pereira on 07/06/2020.
//  Copyright © 2020 Tiago Pereira. All rights reserved.
//

import SwiftUI

struct AnimalDetailView: View {
    @Environment(\.imageCache) var cache: ImageCache
    
    var animal: Animal
    var animalExtraInfo: AnimalInfo?
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading, spacing: 10) {
                
                // Header: Basic Info
                HStack {
                    AsyncImage(url: animal.iconURL!,
                               placeholder: RoundedRectangle(cornerRadius: 12),
                               cache: self.cache
                    ).frame(width: 60, height: 60)
                    
                    VStack(alignment: .leading) {
                        Text("\(animal.name)")
                            .font(.headline)
                        
                        Text("\(animal.category ?? "-")")
                            .font(.footnote)
                            .fontWeight(.semibold)
                    }
                    
                    Spacer()
                }
                
                Divider()
                
                // Useful Info
                HStack {
                    // Price
                    VStack(alignment: .center, spacing: 3) {
                        Text("\(animal.sell ?? 0)")
                            .font(.headline)
                        Text("Price")
                            .font(.headline)
                            .foregroundColor(.primaryColor)
                    }
                    .frame(minWidth: 100, minHeight: 100)
                    .background(Color.secondaryBackground)
                    .cornerRadius(10)
                    
                    // Where to catch
                    HStack(alignment: .top) {
                        VStack (alignment: .leading, spacing: 3) {
                            HStack {
                                Text("Catch at")
                                    .font(.headline)
                                Spacer()
                                Image(systemName: "location")
                            }.foregroundColor(.primaryColor)
                            
                            if animal.category == Category.fish.rawValue {
                                Text("\(animal.obtainedFrom ?? "-")") +
                                    Text(" | \(animal.shadow ?? "-") shadow")
                            } else {
                                Text("\(animal.obtainedFrom ?? "-")")
                                    .font(.body)
                                Text("Weather: \(animal.weather ?? "-")")
                                    .font(.footnote)
                            }
                        }
                        
                        Spacer()
                    }
                    .padding()
                    .frame(minHeight: 100)
                    .background(Color.secondaryBackground)
                    .cornerRadius(10)
                }
                
                // Animal Photo
                HStack(alignment: .center) {
                    Spacer()
                    
                    AsyncImage(url: animal.imageURL!,
                               placeholder: RoundedRectangle(cornerRadius: 12),
                               cache: self.cache)
                        .aspectRatio(contentMode: .fit)
                    
                    Spacer()
                }
                .frame(maxHeight: 150)
                .padding()
                .background(Color.secondaryBackground)
                .cornerRadius(10)
                
                // Availability
                //                    Text("Availability: Months")
                //                    Text("Availability: Time")
                //                    Text("Availability (Insect): Weather")
                
                // Future implementations
                // Needs data, can get from https://animalcrossing.fandom.com/
                // But probably manually
                // - Capture Quotes
                // - Museum explanation
                
                HStack(alignment: .center) {
                    VStack(alignment: .leading, spacing: 3) {
                        Text("Capture Quote")
                            .font(.headline)
                        
                        Text("\(animalExtraInfo?.captureQuote ?? "-")")
                    }
                    
                    Spacer()
                }
                .padding()
                .background(Color.secondaryBackground)
                .cornerRadius(10)
                
                HStack(alignment: .center) {
                    VStack(alignment: .leading, spacing: 3) {
                        Text("Blathers Explanation")
                            .font(.headline)
                        
                        Text("\(animalExtraInfo?.blathersDescription ?? "-")")
                            .lineLimit(nil)
                    }
                    
                    Spacer()
                }
                .padding()
                .background(Color.secondaryBackground)
                .cornerRadius(10)
                
            }.padding()
        }
        
    }
}

struct AnimalDetailView_Previews: PreviewProvider {
    static var previews: some View {
        
        let testingInsect = TestingDataManager.testingInsect
        let insectInfo = TestingDataManager.testingInsectExtraInfo
        let testingFish = TestingDataManager.testingFish
        let fishInfo = TestingDataManager.testingFishExtraInfo
        
        return Group {
            AnimalDetailView(animal: testingInsect, animalExtraInfo: insectInfo)
                .environment(\.colorScheme, .light)
            
            AnimalDetailView(animal: testingFish, animalExtraInfo: fishInfo)
                .environment(\.colorScheme, .light)
            
            AnimalDetailView(animal: testingInsect, animalExtraInfo: insectInfo)
                .environment(\.colorScheme, .dark)
        }
    }
}
