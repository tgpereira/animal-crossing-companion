//
//  AnimalCatalogView.swift
//  AC Hunting Companion
//
//  Created by Tiago Pereira on 05/06/2020.
//  Copyright © 2020 Tiago Pereira. All rights reserved.
//

import SwiftUI

/***
 TODO:
 - Add navigation to critter details (modal view)
 - Being able to record which critter you've donated to the museum already from the list and from the detail view
 - Improve cell layout, inspired on care kit interface
 ***/

struct AnimalCatalogView: View {
    @EnvironmentObject var viewModel: AnimalListViewModel
    
    @State private var isShowingAnimalDetails: Bool = false
    @State private var selectedAnimal: Animal?
    
    @State private var selectedAnimalType = 0
    let animalTypeLabels = ["Fishes", "Insects", "Sea Creatures"]
    
    var animals: [Animal] {
        if selectedAnimalType == 0 {
            return viewModel.fishes
        } else if selectedAnimalType == 1 {
            return viewModel.insects
        } else {
            return viewModel.seaCreatures
        }
    }
    
    var body: some View {
        NavigationView {
            List {
                
                Picker(selection: $selectedAnimalType, label: Text("Animal Type")) {
                    ForEach(0 ..< animalTypeLabels.count) { index in
                        Text("\(self.animalTypeLabels[index])")
                    }
                }.pickerStyle(SegmentedPickerStyle())
                
                Section(header: Text("\(animalTypeLabels[selectedAnimalType])")) {
                    ForEach(self.animals
                        .sorted { $0.critterId! < $1.critterId!}) { animal in
                            Button(action: {
                                self.selectedAnimal = animal
                                self.isShowingAnimalDetails = true
                            }) {
                                AnimalInfoRowView(animal: animal)
                            }.buttonStyle(PlainButtonStyle())
                    }
                }
            }
            .listStyle(GroupedListStyle())
            .environment(\.horizontalSizeClass, .regular)
            .navigationBarTitle("Catalog")
        }.sheet(isPresented: $isShowingAnimalDetails) {
            AnimalDetailView(animal: self.selectedAnimal!, animalExtraInfo: self.viewModel.getExtraInfo(for: self.selectedAnimal!))
        }
        
    }
}

struct AnimalCatalogView_Previews: PreviewProvider {
    static var previews: some View {
        let viewModel = AnimalListViewModel(dataManager: TestingDataManager())
        
        return Group {
            AnimalCatalogView()
                .environmentObject(viewModel)
                .environment(\.colorScheme, .light)
            
            AnimalCatalogView()
                .environmentObject(viewModel)
                .environment(\.colorScheme, .dark)
        }
    }
}
